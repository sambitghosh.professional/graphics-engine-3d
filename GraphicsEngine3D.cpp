#include "olcConsoleGameEngine.h"
// This header is used to just create The window 
// on which to perform rasterization and handle user inputs.
// All grphics and projection caclutions are handled
// in the code below.
using namespace std;

struct vec3
{
	float x, y, z;
};

struct triangle
{
	vec3 points[3];
};

struct mesh
{
	vector<triangle> triangles;
};

struct Matrix4x4 {
	float m[4][4] = { 0 };
};

class GraphicsEngine3D : public olcConsoleGameEngine
{

private:

	Matrix4x4 projection_matrix;
	mesh cube;

	void define_cube() {
		cube.triangles = {
		
		// +Z axis face
		{ 0.0f, 0.0f, 0.0f,    0.0f, 1.0f, 0.0f,    1.0f, 1.0f, 0.0f },
		{ 0.0f, 0.0f, 0.0f,    1.0f, 1.0f, 0.0f,    1.0f, 0.0f, 0.0f },

		// +X axis face                                                  
		{ 1.0f, 0.0f, 0.0f,    1.0f, 1.0f, 0.0f,    1.0f, 1.0f, 1.0f },
		{ 1.0f, 0.0f, 0.0f,    1.0f, 1.0f, 1.0f,    1.0f, 0.0f, 1.0f },

		// -Z axis face                                                   
		{ 1.0f, 0.0f, 1.0f,    1.0f, 1.0f, 1.0f,    0.0f, 1.0f, 1.0f },
		{ 1.0f, 0.0f, 1.0f,    0.0f, 1.0f, 1.0f,    0.0f, 0.0f, 1.0f },

		// -X axis face                                                    
		{ 0.0f, 0.0f, 1.0f,    0.0f, 1.0f, 1.0f,    0.0f, 1.0f, 0.0f },
		{ 0.0f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f,    0.0f, 0.0f, 0.0f },

		// +Y axis face                                                     
		{ 0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 1.0f,    1.0f, 1.0f, 1.0f },
		{ 0.0f, 1.0f, 0.0f,    1.0f, 1.0f, 1.0f,    1.0f, 1.0f, 0.0f },

		// -Y axis face                                                   
		{ 1.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 0.0f },
		{ 1.0f, 0.0f, 1.0f,    0.0f, 0.0f, 0.0f,    1.0f, 0.0f, 0.0f },
		};
	}

	void define_ProjectionMatrix() {
		// This is equivalent to camera settings in real life.
		float near_plane = 0.1f;
		float far_plane = 1000.0f;
		float feild_of_view = 90.0f;
		float aspect_ratio = (float) ScreenHeight() / (float) ScreenWidth();
		float fov_radians = 1.0f / tanf(feild_of_view * 0.5f / 180.0f * 3.141f);

		projection_matrix.m[0][0] = aspect_ratio * fov_radians;
		projection_matrix.m[1][1] = fov_radians;
		projection_matrix.m[2][2] = far_plane / (far_plane - near_plane);
		projection_matrix.m[3][2] = (-far_plane * near_plane) / (far_plane - near_plane);
		projection_matrix.m[2][3] = 1.0f;
		projection_matrix.m[3][3] = 0.0f;
	}

	void Vector_x_Matrix(vec3 &i, vec3 &o, Matrix4x4 &m) {

		o.x = i.x * m.m[0][0] + i.y * m.m[1][0] + i.z * m.m[2][0] + m.m[3][0];
		o.y = i.x * m.m[0][1] + i.y * m.m[1][1] + i.z * m.m[2][1] + m.m[3][1];
		o.z = i.x * m.m[0][2] + i.y * m.m[1][2] + i.z * m.m[2][2] + m.m[3][2];
		float w = i.x * m.m[0][3] + i.y * m.m[1][3] + i.z * m.m[2][3] + m.m[3][3];

		if (w != 0.0f)
		{
			o.x /= w; o.y /= w; o.z /= w;
		}
	}

	void draw_cube() {

		define_cube();

		// Stage 1: Go through all triangles in mesh to calculate on them.
		for (triangle tri : cube.triangles)	
		{
			triangle tri_translated;

			tri_translated = tri;

			tri_translated.points[0].z = tri.points[0].z + 0.0f;
			tri_translated.points[1].z = tri.points[1].z + 0.0f;
			tri_translated.points[2].z = tri.points[2].z + 0.0f;


			triangle tri_projected = tri;

			Vector_x_Matrix(tri_translated.points[0], tri_projected.points[0], projection_matrix);
			Vector_x_Matrix(tri_translated.points[1], tri_projected.points[1], projection_matrix);
			Vector_x_Matrix(tri_translated.points[2], tri_projected.points[2], projection_matrix);

			// Scale up the Cube since projected is normalised between -1 and 1

			// move normalisation to between 0 and 2
			tri_projected.points[0].x += 1.0f;
			tri_projected.points[0].y += 1.0f;
			

			tri_projected.points[1].x += 1.0f;
			tri_projected.points[1].y += 1.0f;
			

			tri_projected.points[2].x += 1.0f;
			tri_projected.points[2].y += 1.0f;
			
			// perform scale calculations to put cube in center of screen
			tri_projected.points[0].x *= 0.5f * (float)ScreenWidth();
			tri_projected.points[1].x *= 0.5f * (float)ScreenWidth();
			tri_projected.points[2].x *= 0.5f * (float)ScreenWidth();

			tri_projected.points[0].y *= 0.5f * (float)ScreenHeight();
			tri_projected.points[1].y *= 0.5f * (float)ScreenHeight();
			tri_projected.points[2].y *= 0.5f * (float)ScreenHeight();


			// Stage 2: Draw the triangles on screen
			//			To do this we use DrawTriangle Funtion from the console game engine.

			DrawTriangle(
				tri_projected.points[0].x, tri_projected.points[0].y,
				tri_projected.points[1].x, tri_projected.points[1].y,
				tri_projected.points[2].x, tri_projected.points[2].y,
				PIXEL_SOLID, FG_WHITE
				);
			DrawTriangle(
				tri.points[0].x, tri.points[0].y,
				tri.points[1].x, tri.points[1].y,
				tri.points[2].x, tri.points[2].y,
				PIXEL_SOLID, FG_WHITE
			);
		}
	}

public:
	GraphicsEngine3D()
	{
		m_sAppName = L"3D Graphics Engine";
	}

	bool OnUserCreate() override
	{
		define_cube();
		return true;
	}

	bool OnUserUpdate(float deltaTime) override
	{
		// clear screen
		Fill(0, 0, ScreenWidth(), ScreenHeight(), PIXEL_SOLID, FG_BLACK);

		draw_cube();

		return true;
	}

};

int main()
{
	GraphicsEngine3D run_object;
	if (run_object.ConstructConsole(200,200,3,3)) {
		run_object.Start();
	}
	return 0;
}

